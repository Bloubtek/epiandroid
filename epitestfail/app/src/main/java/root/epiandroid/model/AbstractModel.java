package root.epiandroid.model;

import android.util.Log;

import root.epiandroid.observer.Observable;
import root.epiandroid.observer.Observer;

/**
 * Created by avice_d on 15/01/16.
 */
public abstract class AbstractModel implements Observable {

    private Observer observer = null;

    public void addObserver(Observer obs) {
        observer = obs;
    }

    public void notifyObserver(Object... objs) {
        Log.e("infos", "Observer Notification");
        if (observer != null)
            observer.update(objs);
    }

    public void removeObserver() {
        observer = null;
    }
}
