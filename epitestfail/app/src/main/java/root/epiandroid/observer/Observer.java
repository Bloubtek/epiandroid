package root.epiandroid.observer;

/**
 * Created by hauteb_h on 15/01/16.
 */
public interface Observer {
    public void update(Object... objs);
}
