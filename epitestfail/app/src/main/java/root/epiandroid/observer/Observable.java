package root.epiandroid.observer;

/**
 * Created by hauteb_h on 15/01/16.
 */
public interface Observable {
    public void addObserver(Observer obs);
    public void removeObserver();
    public void notifyObserver(Object... objs);
}